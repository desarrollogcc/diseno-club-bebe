﻿'MODIFICACIONES
'   FECHA   |               NOMBRE                  |                       DESCRIPCIÓN
'13-02-2019 |           Jairo Torres                | Se agrego nueva linea para el punto 5, se agrego fecha de vencimiento en el app.config y 
'                                                     se agrego linea despues de visita pagina web

Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Configuration
Imports System.Configuration.ConfigurationManager
Imports System.IO.Ports
Imports System.Drawing.Imaging
Module Module1
    Private NombreImpresora As String = ""
    Private NumTicket As String = ""
    Private TipoTienda As String = ""
    Private Vigencia As String = ""
    Private log As New Bitacora
    Private Location As Point
    Private newImage As Image
    Sub Main(args() As String)

        log.Log("Incia Proceso Club BB " & Now.ToString("dd/MM/yyyy") & " V.1")
        If Not validatePrinter() Then
            AbortProcesLog()
            Exit Sub
        Else
            If Not IsNothing(args) Then
                If Not ValidateTicketNum(args) Then
                    log.Log("Numero de ticket Vacio.")
                    AbortProcesLog()
                    Exit Sub
                Else
                    log.Log("Numero de ticket Recibido: " & NumTicket)
                    Dim document As New PrintDocument
                    document.PrinterSettings.PrinterName = NombreImpresora
                    log.Log("Impresora Destino: " & NombreImpresora)
                    document.DefaultPageSettings.Landscape = False
                    AddHandler document.PrintPage, AddressOf document_PrintPage
                    document.Print()
                End If
            Else
                log.Log("Argumentos de la ejecución Nulos.")
                AbortProcesLog()
            End If
        End If

    End Sub

    Private Sub document_PrintPage(sender As Object, e As PrintPageEventArgs)


        log.Log("Inicia diseño del ticket")


        DrawHeadrnFooter(e)
        DrawBodyHeader(e)
        DrawImageStore(e)
        DrawBody(e)
        DrawStepToFollow(e)
        DrawNumTicket(e)






    End Sub

    Private Sub DrawNumTicket(e As PrintPageEventArgs)
        Location = New Point(20, 460)
        e.Graphics.DrawString(NumTicket, New Font("Arial", 12, FontStyle.Bold), Brushes.Black, Location)

    End Sub

    Private Sub DrawStepToFollow(e As PrintPageEventArgs)




        Try
            TipoTienda = AppSettings.Get("TipoTienda").ToString
        Catch ex As Exception

        End Try

        If TipoTienda = "DS" Then
            Location = New Point(0, 370)
            e.Graphics.DrawString("1. Visita la página www.miclubdelsol.com", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)
        Else
            Location = New Point(0, 370)
            e.Graphics.DrawString("1. Visita la página www.miclubwoolworth.com", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)

        End If




        Location = New Point(14, 389)
        e.Graphics.DrawString("o acude al módulo en tienda antes del", New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)
        Location = New Point(14, 410)
        e.Graphics.DrawString(Vigencia, New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)

        Location = New Point(1, 432)
        e.Graphics.DrawString("2. Ingresa este código", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)




        Location = New Point(1, 480)
        e.Graphics.DrawString("3. Ingresa tus datos", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)
        Location = New Point(120, 481)
        e.Graphics.DrawString(" para que recibas tu cupón", New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)
        Location = New Point(14, 500)
        e.Graphics.DrawString("del club", New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)

        Location = New Point(1, 519)
        e.Graphics.DrawString("4. Presenta tu cupón", New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)
        Location = New Point(125, 519)
        e.Graphics.DrawString(" en nuestras cajas para que", New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)
        Location = New Point(14, 538)
        e.Graphics.DrawString("obtengas tu 5% de descuento adicional", New Font("Arial", 9, FontStyle.Regular), Brushes.Black, Location)


        Location = New Point(1, 557)
        e.Graphics.DrawString("5. Cupón válido hasta el " & ConfigurationManager.AppSettings.Get("VigenciaCupon").ToString, New Font("Arial", 9, FontStyle.Bold), Brushes.Black, Location)


    End Sub

    Private Sub DrawBody(e As PrintPageEventArgs)
        Location = New Point(5, 180)
        e.Graphics.DrawString("Y OBTENER UN", New Font("Arial", 12, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(-20, 178)
        e.Graphics.DrawString("5", New Font("Arial", 80, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(47, 214)
        e.Graphics.DrawString("%", New Font("Arial", 18, FontStyle.Bold), Brushes.Black, Location)


        Location = New Point(65, 205)
        e.Graphics.DrawString("DE DESCUENTO", New Font("Arial", 18, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(60, 235)
        e.Graphics.DrawString("ADICIONAL", New Font("Arial", 26, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(1, 280)
        e.Graphics.DrawString("EN TODAS TU COMPRAS DEL", New Font("Arial", 14, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(1, 316)
        e.Graphics.DrawString("DEPARTAMENTO DE BEBÉS", New Font("Arial", 14, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(50, 338)
        e.Graphics.DrawString("(EXCEPTO PAÑALES Y ALIMENTOS)", New Font("Arial", 8, FontStyle.Bold), Brushes.Black, Location)

        Location = New Point(97, 355)
        e.Graphics.DrawString("ES MUY SENCILLO", New Font("Arial", 8, FontStyle.Regular), Brushes.Black, Location)


    End Sub

    Private Sub DrawImageStore(e As PrintPageEventArgs)
        Try
            TipoTienda = AppSettings.Get("TipoTienda").ToString
        Catch ex As Exception

        End Try



        'Location = New Point(20, 70)
        'newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "test.png")
        'e.Graphics.DrawImage(newImage, Location)

        If TipoTienda = "DS" Then
            Location = New Point(10, 55)
            newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "miclubds.png")
            e.Graphics.DrawImage(newImage, Location)
        Else
            Location = New Point(10, 55)
            newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "miclubww.png")
            e.Graphics.DrawImage(newImage, Location)
        End If


    End Sub

    Private Sub DrawBodyHeader(e As PrintPageEventArgs)
        Location = New Point(0, 5)
        e.Graphics.DrawString("¡FELICIDADES!", New Font("Arial", 27, FontStyle.Bold), Brushes.Black, Location)
        Location = New Point(3, 48)
        e.Graphics.DrawString("TÚ PUEDES SER PARTE DE", New Font("Arial", 14.5, FontStyle.Bold), Brushes.Black, Location)

    End Sub

    Private Sub DrawHeadrnFooter(e As PrintPageEventArgs)
        Location = New Point(0, 1)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "SplitLine.png")
        e.Graphics.DrawImage(newImage, Location)

        Location = New Point(0, 580)
        newImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory & "SplitLine.png")
        e.Graphics.DrawImage(newImage, Location)

    End Sub

    Private Function validatePrinter() As Boolean
        Try
            NombreImpresora = AppSettings.Get("NombreImpresora").ToString
            Return True
        Catch ex As Exception
            log.Log("Nombre de impresora No Configurado")

            Return False
        End Try

    End Function
    Private Function ValidateTicketNum(args() As String) As Boolean
        Try
            NumTicket = args(0).Split("|")(0).ToString
            Vigencia = args(0).Split("|")(1).ToString
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function
    Private Sub AbortProcesLog()
        log.Log("Proceso Abortado.")
    End Sub
End Module
